# This file is part of the DEPS/graph-includes package
#
# (c) 2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

# DEPS::Object
# A base class for simple hash-based objects that belong to a graph

package DEPS::Object;
use strict;
use warnings;

use base qw(DEPS::Ingredientable);

sub copy {
  my $self = shift;
  my $class = ref $self;

  my $copy = {};

  foreach my $field (keys %$self) {
    $copy->{$field} = $self->{$field};
  }

  bless ($copy, $class);
  return $copy;
}

sub dump {
  my $obj = shift->copy;
  $obj->{ORIGINGGRAPH} = "$obj->{ORIGINGGRAPH}" if defined $obj->{ORIGINGGRAPH};
  use Data::Dumper;
  Dumper $obj;
}

1;
