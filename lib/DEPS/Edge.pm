# This file is part of the DEPS/graph-includes package
#
# (c) 2005,2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package DEPS::Edge;
use strict;
use warnings;

use base qw(DEPS::Object);
use List::Util qw(sum);
use Set::Object qw();
use Carp qw(croak);

# Objects keys of this class are intentionally not locked, since many
# attributes will be used for various purposes, and it probably makes
# no sense to make things more complicated.

sub new {
  my $class = shift;
  my $self = {};

  ($self->{SRC},$self->{DST}) = @_;

  # sanity checks
  croak "DEPS::Edge::new: src not an object"
    unless ref $self->{SRC};
  croak "DEPS::Edge::new: dst not an object"
    unless ref $self->{DST};

  bless ($self, $class);
  return $self;
}

sub weight {
  my $self = shift;

  if (defined $self->{_INGREDIENTS}) {
    return ( 1 + sum map { $_->weight() } $self->{_INGREDIENTS}->members() );
  } else {
    return 1;
  }
}

1;
