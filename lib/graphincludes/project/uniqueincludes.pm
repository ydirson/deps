# This file is part of the DEPS/graph-includes package
#
# (c) 2005,2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package graphincludes::project::uniqueincludes;
use strict;
use warnings;

use base qw(graphincludes::project::default);

use File::Basename qw(basename);

use graphincludes::params;

sub nlevels { return 1; }
sub filelabel {
  my $self = shift;
  my ($file,$level) = @_;
  $file =~ s/^$self->{PFXSTRIP}// if defined $self->{PFXSTRIP};
  if ($level == 0) {
    return $file;
  } else {
    my $filename = basename($file);
    $filename =~ s!\.[^.]*$!!;	# strip suffix
    return $filename;
  }
}

sub getdeps {
  my $self = shift;
  my ($graph) = @_;

  @ARGV = map {$_->{LABEL}} $graph->get_nodes();
 LINE:
  while (<>) {
    if (m!^\s*#\s*include\s*[<"](.+)[>"]!) {
      my $dstfile = $1;

      foreach my $node ($graph->get_nodes) {
	if ( basename($node->{LABEL}) eq basename($dstfile) ) {
	  if ($ARGV ne $node->{LABEL}) { # skip deps to self, they confuse the reduction checker
	    $self->{ROOTGRAPH}->record_edge ($ARGV, $node->{LABEL});
	  }
	  next LINE;
	}
      }

      $self->record_missed_dep ($ARGV, $dstfile);
    }
  }
}

1;
