# This file is part of the DEPS/graph-includes package
#
# (c) 2005,2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package graphincludes::renderer::tulip;

use warnings;
use strict;

use base qw(graphincludes::renderer);
use Hash::Util qw(lock_keys);

use graphincludes::params;

sub new {
  my $class = shift;
  my $self = {};

  bless ($self, $class);
  lock_keys (%$self);
  return $self;
}

sub printgraph {
  my $self = shift;
  my ($graphnode, $nodestylers, $edgestylers) = @_;
  my $graph = eval { defined $graphnode->{DATA} } ? $graphnode->{DATA} : $graphnode;

  # give unique numeric IDs to nodes, starting at 1

  my $nodecount = 0;
  my %nodeids;

  foreach my $node ($graph->get_nodes) {
    my $label = $node->{LABEL};
    unless (defined $nodeids{$label}) {
      $nodecount++;
      $nodeids{$label} = $nodecount;
    }
  }

  # declare nodes

  print '(nodes';
  for (my $i=1; $i<=$nodecount; $i++) {
    print " ", $i;
  }
  print ")\n";

  # set node labels

  print "(property 0 string \"viewLabel\"\n";
  print " (default \"\" \"\" )\n";
  foreach my $node (keys %nodeids) {
    print " (node $nodeids{$node} \"$node\")\n";
  }
  print ")\n";


  # simple edges

  my $edgecount = 0;

  foreach my $file ($graph->get_edge_origins) {
    foreach my $dest ($graph->get_dep_names_from($file)) {
      $edgecount++;
      print "(edge $edgecount ", $nodeids{$file}, ' ', $nodeids{$dest}, ")\n";
    }
  }
}

sub wait {
}

1;
