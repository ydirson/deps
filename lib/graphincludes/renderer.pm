# This file is part of the graph-includes package
#
# (c) 2005 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package graphincludes::renderer;

use warnings;
use strict;

use Carp qw(croak);

sub set_multipage {
  my $self = shift;
  my $class = ref $self;

  croak "$class does not know yet how to output a multipage graph";
}

sub set_outputformat {
  my $self = shift;
  my $class = ref $self;

  croak "$class does not know yet how to set output format";
}

sub set_outputfile {
  my $self = shift;
  my $class = ref $self;

  croak "$class does not know yet how to set output file";
}

1;
